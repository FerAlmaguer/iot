#https://pypi.org/project/FukuML/
import numpy as np
import FukuML.AdaBoostStump as adaboost_stump

import csv, sys, os
from numpy import genfromtxt
sys.path.append('/home/fer/Documentos/iot/')
from Metrics.metrics import perf_measure
from Metrics.classificationMetrics import perf_complex_measure

def callMe(myDSPath, myTPath, file, separador, trainingRangeS, trainingRangeE, funTarget):
    
    adaboost_stump_bc = adaboost_stump.BinaryClassifier()
    adaboost_stump_bc.load_train_data("%s/%s"%(myTPath,file))
    adaboost_stump_bc.set_param(run_t=10)
    adaboost_stump_bc.init_W()
    adaboost_stump_bc.train()#aquí truena
    
    #test_data = '-8.451 7.694 -1.887 1.017 3.708 7.244 9.748 -2.362 -3.618 1'
    
    prediccion=[]
    sniffer = csv.Sniffer()
    with open("%s/%s"%(myDSPath,file)) as f:
        for _ in range(1):
            next(f)
        for line in f:
            data = line.split(sniffer.sniff(line).delimiter)
            prediccion.append(adaboost_stump_bc.prediction(' '.join(data)))
    
    reales=genfromtxt("%s/%s"%(myDSPath,file), delimiter=separador, skip_header=1, usecols = funTarget)
    labels = genfromtxt("%s/%s"%(myTPath,file), delimiter=separador, skip_header=1, usecols = funTarget)
    perf_measure(reales, prediccion, os.path.realpath(__file__), file)
    perf_complex_measure(reales, prediccion, labels, 1, 'binary', None, True, os.path.realpath(__file__), file)