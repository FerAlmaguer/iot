
import numpy as np
import FukuML.LinearRegression as lr

import sys, os, time
from numpy import genfromtxt
sys.path.append('/home/fer/Documentos/iot/')
from Metrics.metrics import perf_measure
from Metrics.classificationMetrics import perf_complex_measure

def fukuMLR(myDSPath, myTPath, file, separador, limit):
    
    lr_bc = lr.BinaryClassifier()
    lr_bc.load_train_data("%s/%s"%(myTPath,file))
    lr_bc.set_param()
    lr_bc.init_W()
    lr_bc.train()
    time.sleep(5)
    #test_data = '-8.451 7.694 -1.887 1.017 3.708 7.244 9.748 -2.362 -3.618 1'
    
    prediccion=[]
    with open("%s/%s"%(myDSPath,file)) as f:
        '''for _ in range(1):
            next(f)'''
        for line in f:
            data = line.split(separador)
            prediccion.append(lr_bc.prediction(' '.join(data)))
            #prediccion.append(lr_bc.prediction(' '.join(data), 'future_data'))
            
    #print(set(prediccion))
    
    y_score=prediccion
    
    reales=genfromtxt("%s/%s"%(myDSPath,file), delimiter=separador, skip_header=0, usecols = limit-1)
    labels = genfromtxt("%s/%s"%(myTPath,file), delimiter=separador, skip_header=0, usecols = limit-1)
    perf_measure(reales, prediccion, os.path.realpath(__file__), file)
    perf_complex_measure(reales, prediccion, labels, 1, 'micro', None, True, y_score, os.path.realpath(__file__), file)
    #aquí no sé que pasa con un DS que truena con average='binary' usando 'micro' en su lugar.
    #https://stackoverflow.com/questions/45890328/sklearn-metrics-for-multiclass-classification