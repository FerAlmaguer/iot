from sklearn.ensemble import AdaBoostClassifier
from numpy import genfromtxt
import sys, os,time
sys.path.append('/home/fer/Documentos/iot/')
from Metrics.metrics import perf_measure
from Metrics.classificationMetrics import perf_complex_measure

def sciKitAdaBoostCLS(myDSPath, myTPath, file, separador, limit):
    training = genfromtxt("%s/%s"%(myTPath,file), delimiter=separador, skip_header=0, usecols = (list(range(limit-1))))
    labels = genfromtxt("%s/%s"%(myTPath,file), delimiter=separador, skip_header=0, usecols = limit-1)
    clf=AdaBoostClassifier()
    #clf=AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),algorithm="SAMME",n_estimators=200)
    clf.fit(training,labels)
    time.sleep(5)
    prediccion = clf.predict(genfromtxt("%s/%s"%(myDSPath,file), delimiter=separador, skip_header=0, usecols = (list(range(limit-1)))))
    reales=genfromtxt("%s/%s"%(myDSPath,file), delimiter=separador, skip_header=0, usecols = limit-1)
    
    #print(set(prediccion))
    
    y_score = clf.decision_function(training)
    
    perf_measure(reales, prediccion, os.path.realpath(__file__), file)
    
    perf_complex_measure(reales, prediccion, labels, 1, 'micro', None, True, y_score,os.path.realpath(__file__), file)