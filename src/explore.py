#!/usr/bin/python

import sys, csv
from itertools import groupby

sniffer = csv.Sniffer()
data=[]
with open(sys.argv[1],'r') as txt:
    for _ in range(int(sys.argv[2])):
        next(txt)
    for line in txt:
        separador=sniffer.sniff(line).delimiter
        data.append(int(line.split(separador)[int(sys.argv[3])].strip("\n")))

dataN=set(data)
data2=[len(list(group)) for key, group in groupby(sorted(data))]

print(list(zip(sorted(dataN),data2)),len(dataN))
