from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel
from numpy import genfromtxt
import numpy as np
from FeatureSelection.master import imprime

def featureSelectionTB(path, file, separador, limit, FSName):
    training = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = (list(range(limit-1))))
    labels = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = limit-1)
    
    clf = ExtraTreesClassifier()
    clf = clf.fit(training, labels)
    clf.feature_importances_
    model = SelectFromModel(clf, prefit=True)
    newData=model.transform(training)
    
    trueNewData=np.insert(newData,newData.shape[1],labels,axis=1)
    imprime(trueNewData,file,FSName)