from sklearn.feature_selection import VarianceThreshold
from numpy import genfromtxt
import numpy as np
from FeatureSelection.master import imprime

def featureSelectionLV(path, file, separador, limit, FSName):
    training = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = (list(range(limit-1))))
    labels = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = limit-1)
    sel = VarianceThreshold(threshold=(.66 * (1 - .66)))
    newData=sel.fit_transform(training)
    trueNewData=np.insert(newData,newData.shape[1],labels,axis=1)
    imprime(trueNewData,file,FSName)