from numpy import genfromtxt
import numpy as np
from FeatureSelection.master import imprime

def featureSelectionNFS(path, file, separador, limit, FSName):
    training = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = (list(range(limit-1))))
    labels = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = limit-1)
    trueNewData=np.insert(training,training.shape[1],labels,axis=1)
    imprime(trueNewData,file,FSName)