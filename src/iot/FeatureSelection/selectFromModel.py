from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LassoCV
from numpy import genfromtxt
import numpy as np
from FeatureSelection.master import imprime

def featureSelectionSFM(path, file, separador, limit, FSName):
    training = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = (list(range(limit-1))))
    labels = genfromtxt("%s/%s"%(path,file), delimiter=separador, skip_header=1, usecols = limit-1)
    
    # We use the base estimator LassoCV since the L1 norm promotes sparsity of features.
    clf = LassoCV()

    # Set a minimum threshold of 0.25
    sfm = SelectFromModel(clf, threshold=0.25)
    test=sfm.fit(training, labels)
    n_features = sfm.transform(training)
    
    # Reset the threshold till the number of features equals two.
    # Note that the attribute can be set directly instead of repeatedly
    # fitting the metatransformer.
    '''while n_features > 4:
        sfm.threshold += 0.1
        X_transform = sfm.transform(training)
        n_features = X_transform.shape[1]'''
    
    #print(test)
    
    trueNewData=np.insert(n_features,n_features.shape[1],labels,axis=1)
    imprime(trueNewData,file,FSName)