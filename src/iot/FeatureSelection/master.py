import numpy as np
from operator import itemgetter

def imprime(arreglo, fName, FSName):
    
    anomalies=0
    for row in arreglo:
        if row[-1] == 1:
            anomalies +=1
    
    arregloN = sorted(arreglo, key=itemgetter(len(arreglo[1])-1),reverse=True)
    
    anomaliesLST = arregloN[:anomalies]
    np.random.shuffle(anomaliesLST)
    normalLST = arregloN[anomalies:]
    np.random.shuffle(normalLST)
    
    fraction=round((len(anomaliesLST)/3)*2)
    
    trainingLST=anomaliesLST[:fraction]
    trainingLST.extend(normalLST[:fraction])
    np.random.shuffle(trainingLST)
    
    testingLST=anomaliesLST[fraction:]
    testingLST.extend(normalLST[fraction:])
    np.random.shuffle(testingLST)
    
    training='/home/fer/Documentos/IoT/iot/Training/%s.%s.csv'%('.'.join(fName.split('.')[:-1]),FSName)
    testing='/home/fer/Documentos/IoT/iot/Testing/%s.%s.csv'%('.'.join(fName.split('.')[:-1]),FSName)
    np.savetxt(training, trainingLST, newline='\n', fmt="%s", delimiter=',')
    np.savetxt(testing, testingLST, newline='\n', fmt="%s", delimiter=',')