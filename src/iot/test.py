'''
Created on 28/05/2018

@author: Fer
'''
#! /usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from os.path import isfile, join
from memory_profiler import memory_usage
from itertools import islice
from Features.fileManager import *
from Features.mensajes import main as mensaje
from Features.mergeResources import main as merge
from Features.missing import main as faltantes
from Features.classificationMetrics import main as classiMetrics
from Features.metrics import main as metri
from Features.rTime import main as rTime
from Features.splitResources import main as splitRes
from FeatureSelection.noFS import featureSelectionNFS as NFS
from FeatureSelection.lowVariance import featureSelectionLV as fSLV
from FeatureSelection.selectFromModel import featureSelectionSFM as fSSFM
from FeatureSelection.treeBased import featureSelectionTB as fSTB
import sys, csv, time, warnings

sys.path.append('/home/fer/Documentos/IoT/iot/Library/scikit-learn')
from AnomalyDetection.adaBoostClassifier import sciKitAdaBoostCLS as pAdaBoostCLS
from AnomalyDetection.baggingClassifier import scikitBaggingCLS as pBaggingCLS
from AnomalyDetection.extraTreesClassifier import scikitRandomForest as pExtraTrees
from AnomalyDetection.gradientBoostingClassifier import scikitGradientBoosting as pGradientBoosting
from AnomalyDetection.isolationForest import scikitIsolationForest as pIsolationForest
from AnomalyDetection.randomForestClassifier import scikitRandomForest as pRandomForest
from GaussianProcesses.gaussianProcessClassifier import sciKitGaussianProcessClassifie as pGaussianProcess
from GeneralizedLinearModels.logisticRegression import sciKitLogisticRegression as pLogisticRegression
from GeneralizedLinearModels.logisticRegressionCV import  sciKitLogisticRegressionCV as pLogisticRegresionCV
from GeneralizedLinearModels.passiveAggressiveClassifier import sciKitPassiveAggressiveClassifier as pPassiveAggressiveClassifier
from GeneralizedLinearModels.ridgeClassifier import sciKitRidgeClassifier as pRidgeClassifier
from GeneralizedLinearModels.ridgeClassifierCV import sciKitRidgeClassifierCV as pRidgeClassifierCV
from GeneralizedLinearModels.sGDClassifier import sciKitSGDClassifier as pSGDC
from NearestNeighbors.kNeighborsClassifier import sciKitKNeighborsClassifier as pKNeighbors
from NearestNeighbors.radiusNeighborsClassifier import sciKitRadiusNeighborsClassifier as pRadiusNeighbors #Arguments!!!!
from NearestNeighbors.nearestCentroid import sciKitNearestCentroid as pNearestCentroid
from NeuralNetworkModels.mLPClassifier import sciKitMLPClassifier as pMLP
from SVM.linearSVC import sciKitLinearSVC as pSVC
from SVM.nuSVC import sciKitNuSVC as pNuSVC
from SVM.SVC import sciKitSVM as pSVM
from DecisionTrees.decisionTreeClassifier import sciKitDecisionTreeClassifier as pDecisionTree
from DecisionTrees.extraTreeClassifier import sciKitExtraTreeClassifier as pExtraTreeClassifier

divider = "-- * -- + -- * -- + -- * -- + -- * -- + -- * -- + -- * -- + -- * -- +"
myTrainingPath='/home/fer/Documentos/IoT/iot/Training'
myTestPath='/home/fer/Documentos/IoT/iot/Testing'
myFSPath='/home/fer/Documentos/IoT/iot/FeatureSelection'
opDir='/home/fer/Documentos/IoT/iot/OP'
dsDir='/home/fer/Documentos/IoT/iot/DS'
onlyFiles = [f for f in listdir(dsDir) if isfile(join(dsDir, f))]#it's necessary to keep clear the Path from hidden files
ml=[pAdaBoostCLS,pBaggingCLS,pExtraTrees,pGradientBoosting,pIsolationForest,pRandomForest,pGaussianProcess,pLogisticRegression,pLogisticRegresionCV,pPassiveAggressiveClassifier,pRidgeClassifier,pRidgeClassifierCV,pSGDC,pKNeighbors,pRadiusNeighbors,pNearestCentroid,pMLP,pSVC,pNuSVC,pSVM,pDecisionTree,pExtraTreeClassifier]
#
featureSelection=[NFS,fSLV,fSSFM,fSTB]
#
count=len(featureSelection)*len(onlyFiles)

print("'Empezando' en 20 segundos...")
time.sleep(1*15)
print("Empezando en 5 segundos...\n")
time.sleep(1*5)#'''

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

sniffer = csv.Sniffer()
def atributos(args):
    with open(args) as fin:
        for line in islice(fin, 1, 2):
            separador=sniffer.sniff(line).delimiter
            limit = line.count(separador)+1
    return [separador,limit]

countDS = 0
timeLog=open('/home/fer/Documentos/IoT/iot/OP/Rtime.csv','a')
eprint(divider,time.time(),'\n')
mensaje("Starting %f"%time.time())
for f in onlyFiles:
    for fs in featureSelection:
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            warnings.filterwarnings("ignore", category=ResourceWarning, message="unclosed.*<ssl.SSLSocket.*>")
            try:
                separador,limit=atributos("%s/%s"%(dsDir,f))
                fs(dsDir, f, separador, limit, fs.__module__)
                time.sleep(4)
            except Warning as e:
                countDS+=1
                count-=1
                eprint("FS: %s DS: %s\nUnexpected warning: %s %s\n\n%s\n"%(fs.__module__,f,sys.exc_info()[0], e, divider))
                mensaje("Unexpected warning FS on %i/%i %f"%(countDS,count,time.time()))
                print('DS: %i\n\n%s\n'%(countDS,divider))
                continue#'''
            except Exception as e:
                countDS+=1
                count-=1
                eprint("FS: %s DS: %s\nUnexpected exception: %s %s\n\n%s\n"%(fs.__module__,f,sys.exc_info()[0], e, divider))
                mensaje("Unexpected exception FS on %i/%i %f"%(countDS,count,time.time()))
                print('DS: %i\n\n%s\n'%(countDS,divider))
                continue
        fN="%s.%s.csv"%('.'.join(f.split('.')[:-1]),fs.__module__)
        countDS+=1
        print("DS: %i\n"%countDS)
        count-=1
        countML=0
        for cls in ml:
            countML+=1
            print ("R: %s, %s, %s ML: %i/%i/%i"%(cls.__module__, cls.__name__, fN, countML,len(ml),count))
            with warnings.catch_warnings():
                warnings.filterwarnings('error')
                warnings.filterwarnings("ignore", category=ResourceWarning, message="unclosed.*<ssl.SSLSocket.*>")
                try:
                    separador,limit=atributos("%s/%s"%(myTrainingPath,fN))
                    start_time = time.time()
                    cls(myTestPath, myTrainingPath, fN, separador, limit)#el primer path es de test y el segundo es de entrenamiento
                except (FileNotFoundError, IOError) as e:
                    eprint("ML%i: %s DS: %s\nUnexpected IOError: %s %s\n"%(countML,cls.__module__,fN,sys.exc_info()[0],e))
                    mensaje("IOError error on %i/%i/%i %f"%(countML, len(ml),count,time.time()))
                except Warning as e:
                    eprint("ML%i: %s DS: %s\nUnexpected warning: %s %s\n"%(countML,cls.__module__,fN,sys.exc_info()[0], e))
                except Exception as e:
                    eprint("ML%i: %s DS: %s\nUnexpected error: %s %s\n"%(countML,cls.__module__,fN,sys.exc_info()[0],e))
                    mensaje("Unexpected error on %i/%i/%i %f"%(countML,len(ml),count,time.time()))
                finally:
                    timeLog.write("%s.py,%s,%f,%f\n"%('.'.join(cls.__module__.split('.')[1:]),fN,start_time,time.time()))
                    print ("D: %s, %s, %s\n"%(cls.__module__, cls.__name__, fN))
                    time.sleep(4)
                    #mensaje("%i to go"%count)#'''
        timeLog.flush()
        eprint("%s\n"%divider)
        print ("%s\n"%divider)
    
timeLog.close()
eprint(time.time(),'\n')
mensaje("Done! %f"%time.time())
merge(opDir)
faltantes(opDir, len(ml))

classiMetrics(opDir)
metri(opDir)
rTime(opDir)
splitRes(opDir)
print('Done!\n')

#clear && python3.6 Documentos/IoT/iot/src/iot/test.py 2>> Documentos/IoT/iot/OP/test.output
#top -c -p $(pgrep -d',' "python3.6|eclipse")
#sudo renice -n -20 -p 
#while sleep 1; do ./Resources.sh P I D; done >> Documentos/IoT/iot/OP/top-output.csv
#    top -b -n1 | grep "$1" | awk '{print $9 "," $10}' | tr "\n" "," && date +"%s.%7N"
