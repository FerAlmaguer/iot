
missingLST=[]
def missing(dir,file, mlLength):
    f = open ('%s/%s'%(dir,file),'r')
    line=f.readline()
    line=f.readline()
    i=0
    while line:
        current=line.split(',')
        if len(current) == 1:
            break
        currentML=current[0]
        currentDS=timeLST[i][1]
        if currentML == timeLST[i][0]:
            line=f.readline()
        else:
            missingLST.append('%s,%s,%s\n'%(timeLST[i][0],currentDS,file))
        i+=1
    f.close()

timeLST=[]
def main(dir, mlLength):
    with open('%s/Rtime.csv'%dir,'r') as time:
        for _ in range(1):
            next(time)
        for line in time:
            timeLST.append(line.split(','))
    
    for csvFile in['classificationMetrics.csv','metrics.csv']:
        missing(dir,csvFile, mlLength)
    
    with open('%s/missings.csv'%dir,'w') as match:
        match.write("Script,DS,file\n")
        for row in missingLST:
            match.write(row)
    
    print('Missings done!\n')