
def withinScope(resources,timeI, timeF):
    if(resources<(timeF+1)):
        return True
        '''if ((resources-time)>-1 and (time-resources)<1):
            return True
        else:
            return False'''
    else:
        return False

def main(dir):
    timeLST=[]
    with open('%s/Rtime.csv'%dir,'r') as time:
        for _ in range(1):
            next(time)
        for line in time:
            timeLST.append(line.split(','))
    
    resourcesLST=[]
    with open ('%s/top-output.csv'%dir,'r') as resources:
        for _ in range(1):
            next(resources)
        j=0
        for line in resources:
            currenTime=float(line.split(',')[-1])
            matchI=float(timeLST[j][2])
            matchF=float(timeLST[j][3])
            if (withinScope(currenTime,matchI, matchF)):
                resourcesLST.append("%s,%s,%s"%(timeLST[j][0],timeLST[j][1],line))
            else:
                j+=1
                if j<len(timeLST):
                    resourcesLST.append("%s,%s,%s"%(timeLST[j][0],timeLST[j][1],line))
                else:
                    break
                
    with open('%s/resources.csv'%dir,'w') as match:
        match.write("Script,DS,%CPU,%MEM,Time\n")
        for row in resourcesLST:
            match.write(row)
    
    print('Merge resources done!\n')