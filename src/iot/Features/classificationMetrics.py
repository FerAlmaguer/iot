
def splitML(dir):
    for ml in mlLST:
        with open('%s/classificationMetrics.csv'%dir,'r') as metric:
            with open ('%s/classificationMetrics/%s.csv'%(dir,ml),'w') as f:
                f.write('DS,Precision,Recall,F1,Accuracy, auROC, Precision-Recall, log_loss, Kappa\n')
                for line in metric:
                    linea=line.split(',')
                    if linea[0] == ml:
                        f.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,\n'%(linea[1],linea[2],linea[3],linea[4],linea[5],linea[6],linea[7],linea[8],linea[9]))
                        
def splitMetrics(dir,file,column):
    for ds in dsLST:
        with open('%s/classificationMetrics.csv'%dir,'r') as metric:
            with open ('%s/classificationMetrics/%s.%s'%(dir,file,ds),'w') as f:
                if file == 'accuracy' or file == 'auROC' or file == 'precision-recall' or file == 'log_loss' or file == 'Kappa':
                    f.write('Script,%s\n'%file)
                    for line in metric:
                        linea=line.split(',')
                        if ds == linea[1]:
                            f.write('%s,%s\n'%(linea[0],linea[column]))
                else:
                    f.write('Script,F1,%s\n'%file)
                    for line in metric:
                        linea=line.split(',')
                        if ds == linea[1]:
                            f.write('%s,%s,%s\n'%(linea[0],linea[4],linea[column]))

def splitDS(dir):
    for ds in dsLST:
        with open('%s/classificationMetrics.csv'%dir,'r') as metric:
            with open ('%s/classificationMetrics/%s'%(dir,ds),'w') as f:
                f.write('Script,Precision,Recall,F1,Accuracy,auROC,Precision-Recall,log loss,Kappa\n')
                for line in metric:
                    linea=line.split(',')
                    if linea[1] == ds:
                        f.write('%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(linea[0],linea[2],linea[3],linea[4],linea[5],linea[6],linea[7],linea[8],linea[9]))

rTimeLST=[]
mlLST=[]
dsLST=[]
def main(dir):
    with open('%s/Rtime.csv'%dir,'r') as rTime:
        for _ in range(1):
            next(rTime)
        for line in rTime:
            rTimeLST.append(line.split(','))
    
    i=0
    for ds in rTimeLST:
        if rTimeLST[i][1] not in dsLST:
            dsLST.append(rTimeLST[i][1])
        i+=1
        
    i=0
    for ml in rTimeLST:
        if rTimeLST[i][0] not in mlLST:
            mlLST.append(rTimeLST[i][0])
        i+=1
    
    i=2
    for row in ['precision','recall','f1','accuracy','auROC','precision-recall','log_loss','Kappa']:
        if row == 'f1':
            i+=1
        else:
            splitMetrics(dir, row, i)
            i+=1
        
    splitML(dir)
    splitDS(dir)
    
    print('Classification metrics done!\n')

#f3R@iO7:~/Documentos/iot/src/iot$ clear && python3.5 -c 'from Features.classificationMetrics import main; main("/home/fer/Documentos/iot/OP/FS.SandBox1")'
