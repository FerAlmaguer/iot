#!/usr/bin/python

import sys

file='classificationMetrics.csv'
dir=sys.argv[1]
uno=sys.argv[2]
dos=sys.argv[3]
tres=sys.argv[4]

firstData=[]
secondData=[]
thirdData=[]

info=[]

with open("%s/%s/%s"%(dir,uno,file),'r') as first:
    for _ in range(1):
        next(first)
    for line in first:
        firstData.append(line.split(','))
        
with open("%s/%s/%s"%(dir,dos,file),'r') as second:
    for _ in range(1):
        next(second)
    for line in second:
        secondData.append(line.split(','))
        
with open("%s/%s/%s"%(dir,tres,file),'r') as third:
    for _ in range(1):
        next(third)
    for line in third:
        thirdData.append(line.split(','))

for element in range(0,len(firstData)):
    if firstData[element][0] == secondData[element][0] and secondData[element][0] == thirdData[element][0] and firstData[element][1] == secondData[element][1] and secondData[element][1] == thirdData[element][1]:
        try:
            precision= (float(firstData[element][2])+float(secondData[element][2])+float(thirdData[element][2]))/3
        except Exception as e:
            precision = 'Err'
        
        try:
            recall= (float(firstData[element][3])+float(secondData[element][3])+float(thirdData[element][3]))/3
        except Exception as e:
            recall = 'Err'
        
        try:
            f1= (float(firstData[element][4])+float(secondData[element][4])+float(thirdData[element][4]))/3
        except Exception as e:
            f1= 'Err'
        
        try:
            accuracy= (float(firstData[element][5])+float(secondData[element][5])+float(thirdData[element][5]))/3
        except Exception as e:
            accuracy = 'Err'
        
        try:
            auROC= (float(firstData[element][6])+float(secondData[element][6])+float(thirdData[element][6]))/3
        except Exception as e:
            auROC= 'Err'
        
        try:
            p_r= (float(firstData[element][7])+float(secondData[element][7])+float(thirdData[element][7]))/3
        except Exception as e:
            p_r= 'Err'
        
        try:
            log_loss= (float(firstData[element][8])+float(secondData[element][8])+float(thirdData[element][8]))/3
        except Exception as e:
            log_loss= 'Err'
        
        try:
            kappa= (float(firstData[element][9])+float(secondData[element][9])+float(thirdData[element][9]))/3
        except Exception as e:
            kappa = 'Err'
        
        info.append('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(firstData[element][0],firstData[element][1],precision,recall,f1,accuracy,auROC,p_r,log_loss,kappa))
    else:
        print(element+2)
        print(firstData[element][0],firstData[element][1])
        print(secondData[element][0],secondData[element][1])
        print(thirdData[element][0],thirdData[element][1])
        break
    
    with open("%s/average.csv"%dir,'w') as average:
        average.write('Script,DS,Precision,Recall,F1,Accuracy,auROC,Precision-Recall,log_loss,Kappa\n')
        average.write(''.join(info))
        
print('Done!')