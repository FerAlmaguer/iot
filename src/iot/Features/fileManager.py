'''
Created on 28/05/2018

@author: Fer
'''
#! /usr/bin/python
# -*- coding: utf-8 -*-

import os
from os.path import isfile, join
from os import listdir
from itertools import islice

separator = "------------------------------------------------------------------------------------------\n*\t*\t*\t*\t*\t*\t*\t*\t*\t*\t*\t*\n+\t+\t+\t+\t+\t+\t+\t+\t+\t+\t+\t+\n*\t*\t*\t*\t*\t*\t*\t*\t*\t*\t*\t*\n------------------------------------------------------------------------------------------\n\n"

error = "An error has occurred"
message = "I'm in this step"

def training(file,step):
    try:
        with open(file,"r") as f:
            limit= int(sum(1 for line in f)/10)
        with open(file,"r") as f:
            head = list(islice(f, limit))
        fileWrite(file.split("/")[-1],step,head)
        whereAmI()
        return 1
    except UnicodeDecodeError as e:
        pass
    except Exception as e:
        print ("%s, %s %s"%(error, message, step))
        changeDir(step)
        var = input("Please enter path or input file for %s: " %(step))
        training(var,step)
        return 0

def fileWrite(file,step,content):
    myPath='Documentos/iot/Training'
    try:
        f = open("%s/%s"%(myPath,file),"w")
        for i in content:
            f.write(i)
        f.close()
        #bloquear archivo!!!!!!!
        whereAmI()
        return 1
    except Exception as e:
        print ("%s, %s %s"%(error, message, step))
        changeDir(step)
        var = input("Please enter output file for %s: "%(step))
        fileWrite(var,step,content)
        return 0

def makeDir(path,step):
    try:
        os.mkdir(path)
        print("Dir %s/%s made"%(os.getcwd(),path))
        return 1
    except Exception as e:
        print ("%s, %s %s"%(error, message, step))
        changeDir(step)
        var = input("Please enter mkdir name for %s: "%(step))
        makeDir(var,step)
        return 0

def whereAmI():
    print ("I'm here %s\n"%os.getcwd())

def changeDir(step):
    try:
        whereAmI()
        var = input("Please enter dir name you want to go for %s: "%(step))
        os.chdir(var)
        whereAmI()
        return 1
    except Exception as e:
        print(error)
        changeDir(step)
        return 0

if __name__ == '__main__':
    myPath='Documentos/iot/DS'
    onlyFiles = [f for f in listdir(myPath) if isfile(join(myPath, f))]
    for f in onlyFiles:
        training("%s/%s"%(myPath,f),'Training separation')
