
def split(dir):
    for ds in dsLST:
        for ml in mlLST:
            with open('%s/resources.csv'%dir,'r') as res:
                with open ('%s/resources/%s.%s'%(dir,ml,ds),'w') as f:
                    f.write('Time,%CPU,%MEM\n')
                    for line in res:
                        linea=line.split(',')
                        if linea[0] == ml and linea[1] == ds:
                            f.write('%s,%s,%s\n'%(linea[4].strip("\n"),linea[2],linea[3]))

rTimeLST=[]
mlLST=[]
dsLST=[]
def main(dir):
    with open('%s/Rtime.csv'%dir,'r') as rTime:
        for _ in range(1):
            next(rTime)
        for line in rTime:
            rTimeLST.append(line.split(','))
    
    i=0
    for ds in rTimeLST:
        if rTimeLST[i][1] not in dsLST:
            dsLST.append(rTimeLST[i][1])
        i+=1
        
    i=0
    for ml in rTimeLST:
        if rTimeLST[i][0] not in mlLST:
            mlLST.append(rTimeLST[i][0])
        i+=1
        
    split(dir)
    
    print('Split resources done!\n')

#f3R@iO7:~/Documentos/iot/src/iot$ clear && python3.5 -c 'from Features.splitResources import main; main("/home/fer/Documentos/iot/OP/FS.SandBox1")'
