import os, csv

def isPrime(num):
        
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                return False
        else:
            return True
    else:
        return False

n=1
info=[]
def junta(id,data):
    
    if data[1]=='Copelabs12':
        id2=12
    elif data[1]=='Pm':
        id2=0
    else:
        id2=data[1][-1]
    
    if float(data[2]) > (60/n) and isPrime(int(id)+int(id2)):
        anomaly = 1
    else:
        anomaly = 0
        
    info.append('%s,%s,%s,%s,%s,%s,%s\n'%(id,id2,data[2],data[3],data[4],data[5].strip("\n"),anomaly))

myTpaht = '/home/fer/Descargas/copelabs.usense/NSense_Traces_Set2_CRAWDAD/'
def main():
    sniffer = csv.Sniffer()
    for folder in ['Copelabs1','Copelabs2','Copelabs3','Copelabs4','Copelabs5','Copelabs6','Copelabs7','Copelabs8','Copelabs12']:
        print(folder)
        with open('%s%s/Source/SocialStrength.dat'%(myTpaht,folder),'r') as txtFile:
            for line in txtFile:
                separador=sniffer.sniff(line).delimiter
                data=line.split(separador)
                if folder=='Copelabs12':
                    id=12
                else:
                    id=folder[-1]
                junta(id,data)
    with open('%s/copelabs.usense.all.csv'%myTpaht,'w') as merge:
        merge.write('DeviceName, DeviceName, Encounter Duration, Average Encounter Duration, Social Strength (Per hour), Social Strength(Per minute) towards <DeviceName>,Anomaly\n')
        merge.write(''.join(info))
    
    print('Done!\n')
    
main()
