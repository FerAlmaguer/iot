from os.path import isfile, join
import os, csv

n=100 #tope
info=[]
def junta(id,position,data):
    if position == 'pub':
        time=4/n
    elif position == 'ShopWindow':
        time=0.3333/n
    elif position == 'SuperMarket':
        time=2/n
    elif position == 'CommercialCenter2' or position == 'CommercialCenter':
        time=3/n
    elif position == 'CollegePorter':
        time=0.3333/n
    elif position == 'LabReception':
        time=0.3333/n
    elif position == 'dat':
        time=4/n
    else:
        time='a'

    if float(data[2]) - float(data[1]) > (60*60*time):
        anomaly = 1
    else:
        anomaly = 0
    info.append('%s,%s,%s,%s,%s\n'%(id,data[0],data[1],data[2].strip("\n"),anomaly))

myTpaht = '/home/fer/Descargas/upmc.content/'
def main():
    sniffer = csv.Sniffer()
    for folder in ['LR-2mins','SR-6mins-FixLocation','SR-10mins-FixLocation','SR-10mins-Students']:
        onlyFiles = [f for f in os.listdir('%s%s'%(myTpaht,folder)) if isfile(join('%s%s'%(myTpaht,folder), f))]
        for f in onlyFiles:
            linea=f.split('.')
            id = linea[0]
            print(id)
            position = linea[1]
            print(position)
            with open('%s%s/%s'%(myTpaht,folder,f),'r') as txtFile:
                for line in txtFile:
                    separador=sniffer.sniff(line).delimiter
                    data=line.split(separador)
                    junta(id,position,data)
    with open('%s/upmc.content.all.csv'%myTpaht,'w') as merge:
        merge.write('iMote,id,FTime,LTime,Anomaly\n')
        merge.write(''.join(info))
    
    print('Done!\n')
    
main()
