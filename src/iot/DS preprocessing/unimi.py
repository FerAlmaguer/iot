from os.path import isfile, join
import os, csv
from builtins import int

def isPrime(num):
        
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                return False
        else:
            return True
    else:
        return False

info=[]
def main():
    sniffer = csv.Sniffer()
    with open('/home/fer/Descargas/unimi.pmtr/pmtr.txt','r') as txtFile:
        for _ in range(0):
            next(txtFile)
        for line in txtFile:
            separador=sniffer.sniff(line).delimiter
            data=line.split(separador)
            if isPrime(int(data[1])) and int(data[3]) - int(data[2]) > 120/121: #tope
                anomaly=1
            else:
                anomaly=0
            info.append('%s,%s,%s,%s,%s\n'%(data[0],data[1],data[2],data[3].strip('\n'),anomaly))
    with open('/home/fer/Descargas/unimi.pmtr/unimi.pmtr.anomaly.txt','w') as file:
        file.write('Id,iD,dateI,dateE,Anomaly\n')
        file.write(''.join(info))
    
    print('Done!\n')
    
main()
