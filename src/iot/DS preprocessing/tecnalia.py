import csv, datetime

myTpaht = '/home/fer/Descargas/tecnalia.humanet'
info=[]
def main():
    sniffer = csv.Sniffer()
    with open('%s/%s'%(myTpaht,'t_encounters.csv'),'r') as txtFile:
        for _ in range(1):
            next(txtFile)
        for line in txtFile:
            separador=sniffer.sniff(line).delimiter
            data=line.split(separador)
            
            id=int(data[0],16)
            id2=int(data[1],16)
            
            hour=data[2].split(':')
            date=data[3].split('-')
            iTime=datetime.datetime(int(date[0]),int(date[1]),int(date[2]),int(hour[0]),int(hour[1]),int(hour[2])).strftime('%s')
            
            hour2=data[4].split(':')
            date2=data[5].split('-')
            fTime=datetime.datetime(int(date2[0]),int(date2[1]),int(date2[2]),int(hour2[0]),int(hour2[1]),int(hour2[2])).strftime('%s')
            
            if (int(fTime) - int(iTime)) >= (10/4) and int(data[6]) == 1: # 3 o 4 no hay diferencia
                anomaly = 1
            else:
                anomaly = 0
            
            info.append('%s,%s,%s,%s,%s,%s\n'%(id,id2,iTime,fTime,data[6].strip("\n"),anomaly))
            
    with open('%s/%s'%(myTpaht,'tecnalia.humanet.csv'),'w') as txtAnomaly:
        txtAnomaly.write('dev1,dev2,date_init,date_end,state,Anomaly\n')
        txtAnomaly.write(''.join(info))
    print('Done!\n')
    
main()