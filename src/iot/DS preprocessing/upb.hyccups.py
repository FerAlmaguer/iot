import os, csv

def isPrime(num):
        
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                return False
        else:
            return True
    else:
        return False

info=[]
def junta(data):
    
    if float(data[3]) > (281000) and isPrime(int(data[0])*int(data[1])):
        anomaly = 1
    else:
        anomaly = 0
        
    info.append('%s,%s,%s,%s,%s\n'%(data[0],data[1],data[2],data[3].strip("\n"),anomaly))

myTpaht = '/home/fer/Descargas/upb.hyccups/'
def main():
    sniffer = csv.Sniffer()
    with open('%sfull_output.txt'%(myTpaht),'r') as txtFile:
        for line in txtFile:
            separador=sniffer.sniff(line).delimiter
            data=line.split(separador)
            junta(data)
    with open('%s/upb.hyccups.anomalies.csv'%myTpaht,'w') as merge:
        merge.write('ID, id, Time, -,Anomaly\n')
        merge.write(''.join(info))
    
    print('Done!\n')
    
main()
