from os.path import isfile, join
import os, csv

myTpaht = '/home/fer/Descargas/cu.rssi'
def main():
    sniffer = csv.Sniffer()
    onlyFiles = [f for f in os.listdir(myTpaht) if isfile(join(myTpaht, f))]
    for f in onlyFiles:
        print(f)
        with open('%s/%s'%(myTpaht,f),'r') as txtFile:
            for line in txtFile:
                separador=sniffer.sniff(line).delimiter
                data=line.split(separador)
                if data[2]=='dev1':
                    dev=1
                else:
                    dev=2
                if data[3]=='down':
                    space = 1
                elif data[3]=='left':
                    space = 2
                elif data[3]=='right':
                    space = 3
                elif data[3]=='up':
                    space = 4
                else:
                    space = data[3]
                if data[3]=='up' and int(data[6]) <= 0 and int(data[7]) <= 0 and int(data[8]) <= 0 and int(data[9]) <= 0 and int(data[10]) <= 0:
                    anomaly = 1
                else:
                    anomaly = 0
                with open('%s/%s.csv'%(myTpaht,f),'a') as txtFile:
                    txtFile.write('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n'%(data[0],data[1],dev,space,data[4],data[5],data[6],data[7],data[8],data[9],data[10].strip("\n"),anomaly))
    print('Done!\n')
    
main()

#cat 1.txt 2.txt 3.txt > 0.txt