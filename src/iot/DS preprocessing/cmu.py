from os.path import isfile, join
import os, csv
from math import sqrt
from itertools import count, islice

def anomaly(data,separador):
    with open('/home/fer/Descargas/cmu_supermarket_anomaly.csv','a') as merge:
        if float(data[0]) < 0 or float(data[2]) < 0:
            anomaly = 1
        else:
            anomaly = 0
        newData=[x if x != 'NaN\n' else '0' for x in data]
        tNewData=[x if x != 'NaN' else '0' for x in newData]
        merge.write('%s,%s\n'%(separador.join(tNewData).strip('\n'),anomaly))

def main():
    sniffer = csv.Sniffer()
    with open('/home/fer/Descargas/cmu_supermarket.csv','r') as txtFile:
        for _ in range(1):
            next(txtFile)
        for line in txtFile:
            separador=sniffer.sniff(line).delimiter
            data=line.split(separador)
            anomaly(data,separador)
    
    print('Done!\n')
    
main()
