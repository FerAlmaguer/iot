from os.path import isfile, join
import os, csv
from builtins import int

def isPrime(num):
        
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                return False
        else:
            return True
    else:
        return False

def junta(site,person,data):
    if float(data[1]) < 0 and float(data[2]) < 0 and isPrime(int(person)):
        anomaly = 1
    else:
        anomaly = 0
        
    if site == 'KAIST':
        siteN = 1
    elif site == 'NCSU':
        siteN=2
    elif site == 'NewYork':
        siteN=3
        if float(data[1]) < 0 and float(data[2]) < 0 and not isPrime(int(person)):
            anomaly=1
        else:
            anomaly=0
    elif site == 'Orlando':
        siteN=4
    elif site == 'Statefair':
        siteN=5
    else:
        siteN='a'
        
    with open('%s/%s.csv'%(myTpaht,site),'a') as merge:
        merge.write('%s,%s,%s,%s,%s\n'%(siteN,person,data[1],data[2],anomaly))

myTpaht = '/home/fer/Descargas/ncsu.mobilitymodels'
def main():
    sniffer = csv.Sniffer()
    for site in ['KAIST','NCSU','NewYork','Orlando','Statefair']:
        onlyFiles = [f for f in os.listdir('%s/%s'%(myTpaht,site)) if isfile(join('%s/%s'%(myTpaht,site), f))]
        print('%s/%s'%(myTpaht,site))
        for f in onlyFiles:
            person = f.split('_')[2].split('.')[0]
            print(person)
            with open('%s/%s/%s'%(myTpaht,site,f),'r') as txtFile:
                for line in txtFile:
                    separador=sniffer.sniff(line).delimiter
                    data=line.split(separador)
                    junta(site,person,data)
    
    print('Done!\n')
    
main()
