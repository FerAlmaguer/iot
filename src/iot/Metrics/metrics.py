import time

def perf_measure(y_actual, y_hat, osPathFile, DS):
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==1:
           TP += 1
        if y_hat[i]==1 and y_actual[i]!=y_hat[i]:
           FP += 1
        if y_actual[i]==y_hat[i]==0:
           TN += 1
        if y_hat[i]==0 and y_actual[i]!=y_hat[i]:
           FN += 1

    with open('/home/fer/Documentos/IoT/iot/OP/metrics.csv','a') as f:
        f.write("%s,%s,%s,%s,%s,%s,%f\n"%(osPathFile.rsplit('/', 1)[-1], DS,TP, FP, TN, FN,time.time()))