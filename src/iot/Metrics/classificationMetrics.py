from sklearn import metrics
import time

def perf_complex_measure(y_true, y_pred, labels, pos_label, average, sample_weight, normalize, y_score, osPathFile, file):
    
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    
    y_actual=y_true
    y_hat=y_pred
    
    for i in range(len(y_hat)): 
        if y_actual[i]==y_hat[i]==1:
           TP += 1
        if y_hat[i]==1 and y_actual[i]!=y_hat[i]:
           FP += 1
        if y_actual[i]==y_hat[i]==0:
           TN += 1
        if y_hat[i]==0 and y_actual[i]!=y_hat[i]:
           FN += 1
    
    try:
        precision=(TP/(TP+FP))
    except ZeroDivisionError as e:
        precision = 'Err'
    
    try:
        recall=(TP/(TP+FN))
    except ZeroDivisionError as e:
        recall='Err'
    
    try:
        FPR=(FP/(FP+TN))
    except ZeroDivisionError as e:
        FPR='Err'
    
    try:
        f1=((2*(precision*recall))/(precision+recall))
    except Exception as e:
        f1='Err'
    
    try:
        accuracy=((TP+TN)/(TP+TN+FP+FN))
    except ZeroDivisionError as e:
        accuracy='Err'
    
    y_score = y_pred
    
    averagePrecicion=metrics.average_precision_score(y_true, y_score, average, sample_weight)
    try:
        auROC=((1+recall-FPR)/2)
    except TypeError as e:
        auROC='Err'
    log=metrics.log_loss(y_true, y_pred)
    kappa=metrics.cohen_kappa_score(y_true,y_pred)
    
    with open('/home/fer/Documentos/IoT/iot/OP/classificationMetrics.csv','a') as f:
        f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%f\n"%(osPathFile.rsplit('/', 1)[-1], file, precision,recall,f1,accuracy,auROC,averagePrecicion,log,kappa,time.time()))
#http://scikit-learn.org/stable/modules/classes.html#sklearn-metrics-metrics '''