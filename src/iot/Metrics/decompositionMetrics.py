from sklearn import metrics
import numpy as np

def perf_complex_measure(title, reales, prediccion, osPathFile, file):
    
    density = len(np.flatnonzero(reales))
    squared_error=np.sum((prediccion - reales) ** 2)
    
    with open('/home/fer/Documentos/iot/OP/decompositionMetrics.csv','a') as f:
        f.write('%s,%s,%s,%s,%.2f\n'% (osPathFile,file,title, density, squared_error))